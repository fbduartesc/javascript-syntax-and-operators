# JavaScript Syntax and Operators

JavaScript Syntax and Operators
by Paul D. Sheriff

Learn various JavaScript structures such as switch, for/in, and for/of. See what math, logical, and comparison operators you can use. Explore how to handle exceptions and work with the 'this' keywor